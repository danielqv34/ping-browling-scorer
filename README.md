**Solution objective**
The goal of this exercise is a command-line application to score a game of ten-pin bowling

**Domain**

Calculate score a game of ten-pin bowling

**Game Rules**

The program should read the input text file and parse its content, 
which should have the results for several players bowling 10 frames each, 
written according to the following :


*  Each line represents a player and a chance with the subsequent number of pins 
knocked down.

*  An 'F' indicates a foul on that chance and no pins knocked down
(identical for scoring to a roll of 0).
c. The rows are tab-separated.

*  The program should handle bad input like more than 
ten throws (i.e., no chance will produce a negative number of knocked down pins 
or more than 10, etc), invalid score value or incorrect format

*  For each player, print their name on a separate line before printing that player's pinfalls and score.

*  All values are tab-separated.

*  The output should calculate if a player scores a strike ('X'), a spare ('/') and allow
for extra chances in the tenth frame.


**Input**
 command-line text file as input separate by tab
 
**Bonus**
    This software solution uses Java 8 streams and lambdas with JUnit for Testing
    with lombook library,
    
**Valid Input File**

Jeff	10
John	3
John	7
Jeff	7
Jeff	3
John	6
John	3
Jeff	9
Jeff	0
John	10
Jeff	10
John	8
John	1
Jeff	0
Jeff	8
John	10
Jeff	8
Jeff	2
John	10
Jeff	F
Jeff	6
John	9
John	0
Jeff	10
John	7
John	3
Jeff	10
John	4
John	4
Jeff	10
Jeff	8
Jeff	1
John	10
John	9
John	0

 
 
 
 
 
 
 