package com.dqv.validators;

import com.dqv.exceptions.InputFileInvalidFormat;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 16:06
 */
public class FormatValidator {

    private Validator validator;

    @Before
    public void setup() {
        validator = new InputFileFormatValidator();
    }

    @Test
    public void isValid() {
        String player_scores = "Jeff\t10\n" +
                "Daniel\t5\n" +
                "Daniel\t5\n" +
                "Jeff\t9\n" +
                "Jeff\tF\n" +
                "Daniel\t8\n" +
                "Daniel\t2\n" +
                "Jeff\tX\n" +
                "Daniel\t5\n" +
                "Daniel\t/\n";
        boolean isValid = validator.isValid(player_scores);
        assertTrue(isValid);
    }

    @Test
    public void isNotValid() {
        String player_scores = "Jeff\t10\n" +
                "Daniel\t5\n" +
                "Daniel\t11";

        boolean isValid = validator.isValid(player_scores);
        assertFalse(isValid);
    }

    @Test(expected = InputFileInvalidFormat.class)
    public void invalidFormatException(){
        String player_scores = "Jeff\t10\n" +
                "Daniel\t5\n" +
                "Daniel\t1sadads1";
        validator.isValid(player_scores);
    }
}
