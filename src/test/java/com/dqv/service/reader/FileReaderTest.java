package com.dqv.service.reader;

import com.dqv.service.fileReaderService.CustomFileReaderService;
import com.dqv.service.fileReaderService.CustomFileReaderServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 14:42
 */
public class FileReaderTest {

    private CustomFileReaderService customFileReaderService;
    private String testResourcesPath;

    @Before
    public void setup(){
        customFileReaderService = new CustomFileReaderServiceImpl();
        testResourcesPath = "src/test/resources/";
    }

    @Test
    public void file() {
        String fileName = "/some/path/to/any_file.txt";
        File file = customFileReaderService.getFile(fileName);
        assertNotNull(file);
        assertEquals(fileName, file.getAbsolutePath());
    }

    @Test
    public void fullRead() throws IOException {
        String fileName = testResourcesPath+"test_file_full_read.txt";
        String expectedContent = "Hello World!";
        fileWriter(fileName, expectedContent);

        String actualContent = customFileReaderService.readFile(new File(fileName));
        assertEquals(expectedContent, actualContent);
    }

    @Test
    public void readFilePerLine() throws IOException {
        String fileName = testResourcesPath+"Players-Round_test.txt";
        String content = "1. First Line\n" +
                "2. Second line\n" +
                "3. Thirst line";
        fileWriter(fileName, content);

        List<String> expectedContent = Arrays.asList(content.split("\n"));
        List<String> actualContent = customFileReaderService.readFilePerLine(new File(fileName));
        assertEquals(expectedContent.size(), actualContent.size());
        for(int i = 0; i < expectedContent.size(); i++){
            assertEquals(expectedContent.get(i), actualContent.get(i));
        }
    }



    private void fileWriter(String filePath, String content) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(new File(filePath));
        fileOut.write(content.getBytes());
        fileOut.close();
    }
}
