package com.dqv.service.parser;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.domain.Player;
import com.dqv.domain.Round;
import com.dqv.service.bowlingGameService.BowlingGameServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 15:31
 */
public class BowlingGameParserImplTest {

    private BowlingGameParser bowlingGameParser;
    private BowlingGameServiceImpl bowlingGameService;
    private String gameSets;


    @Before
    public void setup() {
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGameParser = new BowlingGameParserImpl();
        bowlingGameService = new BowlingGameServiceImpl(bowlingGame);
        gameSets = "Player1\t10\n" +
                "Player2\t3\n" +
                "Player2\t7\n" +
                "Player1\t7\n" +
                "Player1\t3\n" +
                "Player2\t6\n" +
                "Player2\t3\n" +
                "Player1\t9\n" +
                "Player1\t0\n" +
                "Player2\t10\n" +
                "Player1\t10\n" +
                "Player2\t8\n" +
                "Player2\t1\n" +
                "Player1\t0\n" +
                "Player1\t8\n" +
                "Player2\t10\n" +
                "Player1\t8\n" +
                "Player1\t2\n" +
                "Player2\t10\n" +
                "Player1\tF\n" +
                "Player1\t6\n" +
                "Player2\t9\n" +
                "Player2\t0\n" +
                "Player1\t10\n" +
                "Player2\t7\n" +
                "Player2\t3\n" +
                "Player1\t10\n" +
                "Player2\t4\n" +
                "Player2\t4\n" +
                "Player1\t10\n" +
                "Player1\t8\n" +
                "Player1\t1\n" +
                "Player2\t10\n" +
                "Player2\t9\n" +
                "Player2\t0";


        Player player1 = new Player("Player1");
        Player player2 = new Player("Player2");

        bowlingGameService.addPlayerToGame(player1);
        bowlingGameService.addPlayerToGame(player2);


        Frame frame1_Player1 = new Frame(1, player1, Collections.singletonList(new Round("10")));
        Frame frame1_Player2 = new Frame(1, player2, Arrays.asList(new Round("3"), new Round("7")));
        Frame frame2_Player1 = new Frame(2, player1, Arrays.asList(new Round("7"), new Round("3")));
        Frame frame2_Player2 = new Frame(2, player2, Arrays.asList(new Round("6"), new Round("3")));
        Frame frame3_Player1 = new Frame(3, player1, Arrays.asList(new Round("9"), new Round("0")));
        Frame frame3_Player2 = new Frame(3, player2, Collections.singletonList(new Round("10")));
        Frame frame4_Player1 = new Frame(4, player1, Collections.singletonList(new Round("10")));
        Frame frame4_Player2 = new Frame(4, player2, Arrays.asList(new Round("8"), new Round("1")));
        Frame frame5_Player1 = new Frame(5, player1, Arrays.asList(new Round("0"), new Round("8")));
        Frame frame5_Player2 = new Frame(5, player2, Collections.singletonList(new Round("10")));
        Frame frame6_Player1 = new Frame(6, player1, Arrays.asList(new Round("8"), new Round("2")));
        Frame frame6_Player2 = new Frame(6, player2, Collections.singletonList(new Round("10")));
        Frame frame7_Player1 = new Frame(7, player1, Arrays.asList(new Round("F"), new Round("6")));
        Frame frame7_Player2 = new Frame(7, player2, Arrays.asList(new Round("9"), new Round("0")));
        Frame frame8_Player1 = new Frame(8, player1, Collections.singletonList(new Round("10")));
        Frame frame8_Player2 = new Frame(8, player2, Arrays.asList(new Round("7"), new Round("3")));
        Frame frame9_Player1 = new Frame(9, player1, Collections.singletonList(new Round("10")));
        Frame frame9_Player2 = new Frame(9, player2, Arrays.asList(new Round("4"), new Round("4")));
        Frame frame10_Player1 = new Frame(10, player1, Arrays.asList(new Round("10"), new Round("8"), new Round("1")));
        Frame frame10_Player2 = new Frame(10, player2, Arrays.asList(new Round("10"), new Round("9"), new Round("0")));


        bowlingGameService.addFrameToPlayer(frame1_Player1);
        bowlingGameService.addFrameToPlayer(frame1_Player2);
        bowlingGameService.addFrameToPlayer(frame2_Player1);
        bowlingGameService.addFrameToPlayer(frame2_Player2);
        bowlingGameService.addFrameToPlayer(frame3_Player1);
        bowlingGameService.addFrameToPlayer(frame3_Player2);
        bowlingGameService.addFrameToPlayer(frame4_Player1);
        bowlingGameService.addFrameToPlayer(frame4_Player2);
        bowlingGameService.addFrameToPlayer(frame5_Player1);
        bowlingGameService.addFrameToPlayer(frame5_Player2);
        bowlingGameService.addFrameToPlayer(frame6_Player1);
        bowlingGameService.addFrameToPlayer(frame6_Player2);
        bowlingGameService.addFrameToPlayer(frame7_Player1);
        bowlingGameService.addFrameToPlayer(frame7_Player2);
        bowlingGameService.addFrameToPlayer(frame8_Player1);
        bowlingGameService.addFrameToPlayer(frame8_Player2);
        bowlingGameService.addFrameToPlayer(frame9_Player1);
        bowlingGameService.addFrameToPlayer(frame9_Player2);
        bowlingGameService.addFrameToPlayer(frame10_Player1);
        bowlingGameService.addFrameToPlayer(frame10_Player2);

    }

    @Test
    public void parse() {
        Optional<BowlingGame> actualGame = bowlingGameParser.bowlingGameParser(Arrays.asList(gameSets.split("\n")));
        assertTrue(actualGame.isPresent());
        for (Player player : bowlingGameService.getAllPlayers()) {
            assertTrue("Expected player=" + player.getName() + " but get:\n" + actualGame.get().getPlayers(),
                    actualGame.get().getPlayers().contains(player));
        }
        for (Frame expectedFrame : bowlingGameService.getAllFrames()) {
            List<Frame> actualGameFrames = new ArrayList<>(actualGame.get().getFrames());
            int actualSetIndex = actualGameFrames.indexOf(expectedFrame);
            assertNotEquals(-1, actualSetIndex);
            Frame actualSet = actualGameFrames.get(actualSetIndex);
            assertEquals(expectedFrame.getFrameId(), actualSet.getFrameId());
            assertEquals(expectedFrame.getPlayer(), actualSet.getPlayer());
            assertEquals(expectedFrame.getRounds(), actualSet.getRounds());
        }
    }
}
