package com.dqv.service.bowling;

import com.dqv.domain.BowlingGame;
import com.dqv.exceptions.InputFileInvalidFormat;
import com.dqv.service.bowlingGameService.LoadBowlingGameService;
import com.dqv.service.deployService.DeployService;
import com.dqv.service.deployService.DeployServiceImpl;
import com.dqv.service.fileReaderService.CustomFileReaderService;
import com.dqv.service.fileReaderService.CustomFileReaderServiceImpl;
import com.dqv.service.parser.BowlingGameParserImpl;
import com.dqv.service.scoreService.ScoreService;
import com.dqv.service.scoreService.ScoreServiceImpl;
import com.dqv.validators.InputFileFormatValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-30
 * Time: 21:19
 */
public class LoadBowlingGameServiceImplTest {

    private LoadBowlingGameService loadBowlingGameService;
    private CustomFileReaderService customFileReaderService;
    private InputFileFormatValidator inputFileInvalidFormat;
    private BowlingGameParserImpl bowlingGameParser;
    private ScoreService scoreService;
    private DeployService deployService;


    @Before
    public void setUp() {
        this.customFileReaderService = new CustomFileReaderServiceImpl();
        this.inputFileInvalidFormat = new InputFileFormatValidator();
        this.bowlingGameParser = new BowlingGameParserImpl();
        this.scoreService = new ScoreServiceImpl();
        this.deployService = new DeployServiceImpl();
        this.loadBowlingGameService =
                new LoadBowlingGameService(customFileReaderService, inputFileInvalidFormat, bowlingGameParser, scoreService, deployService);

    }

    @Test
    public void initGameTest() {
        Optional<BowlingGame> bowlingGameInit =
                loadBowlingGameService
                        .initGame("/Users/danielqv/Documents/kotlin-bowl/ping-browling-scorer/src/test/resources/valid-file.txt");
        assertTrue(bowlingGameInit.isPresent());

    }

    @Test
    public void initGameCalculatorTest() {
        Optional<BowlingGame> bowlingGameInit =
                loadBowlingGameService
                        .initGame("/Users/danielqv/Documents/kotlin-bowl/ping-browling-scorer/src/test/resources/valid-file.txt");

        BowlingGame initScore = loadBowlingGameService.getGameScores(bowlingGameInit.get());
        assertNotNull(initScore);
    }


    @Test(expected = InputFileInvalidFormat.class)
    public void initGameInvalidFormatTest() {
        Optional<BowlingGame> bowlingGameInit =
                loadBowlingGameService
                        .initGame("/Users/danielqv/Documents/kotlin-bowl/ping-browling-scorer/src/test/resources/Players-Round_test.txt");
        assertTrue(bowlingGameInit.isPresent());

    }

    @Test(expected = InputFileInvalidFormat.class)
    public void initGameCalculatorInvalidFormatTest() {
        Optional<BowlingGame> bowlingGameInit =
                loadBowlingGameService
                        .initGame("/Users/danielqv/Documents/kotlin-bowl/ping-browling-scorer/src/test/resources/Players-Round_test.txt");

        BowlingGame initScore = loadBowlingGameService.getGameScores(bowlingGameInit.get());
        assertNotNull(initScore);
    }
}
