package com.dqv.service.bowling;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.domain.Player;
import com.dqv.domain.Round;
import com.dqv.service.bowlingGameService.BowlingGameService;
import com.dqv.service.bowlingGameService.BowlingGameServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 14:56
 */
public class BowlingServiceImplTest {


    private BowlingGameService bowlingGameService;
    private Player player1;
    private Player player2;
    private Frame frame1;
    private Frame frame2;


    @Before
    public void setup() {
        /**
         * Players content expected*/
        Set<Player> players = new HashSet<>();
        player1 = new Player("Jeff");
        player2 = new Player("Jhon");
        players.add(player1);
        players.add(player2);


        Set<Frame> frames = new LinkedHashSet<>();
        frame1 = new Frame(player1, Collections.singletonList(new Round("10")));
        frame2 = new Frame(player2, Collections.singletonList(new Round("10")));
        frames.add(frame1);
        frames.add(frame2);

        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.setPlayers(players);
        bowlingGame.setFrames(frames);
        bowlingGameService = new BowlingGameServiceImpl(bowlingGame);
    }


    @Test
    public void getAllPlayers() {
        Set<Player> expectedPlayersContent = new HashSet<>();
        expectedPlayersContent.add(player1);
        expectedPlayersContent.add(player2);

        Set<Player> actualPlayersContent = bowlingGameService.getAllPlayers();
        assertEquals(expectedPlayersContent.size(), actualPlayersContent.size());
        assertThat(actualPlayersContent, is(expectedPlayersContent));

    }

    @Test
    public void getAllFrames() {
        Set<Frame> expectedFrameContent = new LinkedHashSet<>();
        frame1 = new Frame(player1, Collections.singletonList(new Round("10")));
        frame2 = new Frame(player2, Collections.singletonList(new Round("10")));
        expectedFrameContent.add(frame1);
        expectedFrameContent.add(frame2);

        Set<Frame> actualFrameContent = bowlingGameService.getAllFrames();
        assertEquals(expectedFrameContent.size(), actualFrameContent.size());
        assertThat(actualFrameContent, is(expectedFrameContent));
    }

    @Test
    public void addPlayerToGame() {
        Player expectedPlayer = player1;
        bowlingGameService.addPlayerToGame(player1);

        Player actualPlayer = bowlingGameService
                .getAllPlayers()
                .stream()
                .findFirst()
                .orElse(new Player());

        assertEquals(expectedPlayer, actualPlayer);

    }

    @Test
    public void addFrameToPlayer() {

        Frame exceptedFrame = new Frame(new Player("Jeff"), Collections.singletonList(new Round("10")));
        Frame frame = new Frame(new Player("Jeff"), Collections.singletonList(new Round("10")));
        bowlingGameService.addFrameToPlayer(frame);

        Frame actualContent = bowlingGameService
                .getAllFrames()
                .stream()
                .findFirst()
                .orElse(new Frame());

        assertEquals(exceptedFrame, actualContent);
    }


}
