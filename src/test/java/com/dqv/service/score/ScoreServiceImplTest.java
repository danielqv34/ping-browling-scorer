package com.dqv.service.score;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.domain.Player;
import com.dqv.domain.Round;
import com.dqv.service.bowlingGameService.BowlingGameService;
import com.dqv.service.bowlingGameService.BowlingGameServiceImpl;
import com.dqv.service.scoreService.ScoreFunctions;
import com.dqv.service.scoreService.ScoreService;
import com.dqv.service.scoreService.ScoreServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 18:41
 */
public class ScoreServiceImplTest {

    private ScoreService scoreService;
    private BowlingGame bowlingGame;
    private BowlingGameService bowlingGameService;
    private final String player1 = "Jeff";
    private final String player2 = "Daniel";

    @Before
    public void before() {
        this.scoreService = new ScoreServiceImpl();
        bowlingGame = new BowlingGame();
        this.bowlingGameService = new BowlingGameServiceImpl(bowlingGame);

        Player jeff = new Player(player1);
        Player daniel = new Player(player2);

        bowlingGameService.addPlayerToGame(jeff);
        bowlingGameService.addPlayerToGame(daniel);

        Frame frame1_Player1 = new Frame(1, jeff, Collections.singletonList(new Round("10")), 20);
        Frame frame2_Player1 = new Frame(2, jeff, Arrays.asList(new Round("7"), new Round("3")), 39);
        Frame frame3_Player1 = new Frame(3, jeff, Arrays.asList(new Round("9"), new Round("0")), 48);
        Frame frame4_Player1 = new Frame(4, jeff, Collections.singletonList(new Round("10")), 66);
        Frame frame5_Player1 = new Frame(5, jeff, Arrays.asList(new Round("0"), new Round("8")), 74);
        Frame frame6_Player1 = new Frame(6, jeff, Arrays.asList(new Round("8"), new Round("2")), 84);
        Frame frame7_Player1 = new Frame(7, jeff, Arrays.asList(new Round("F"), new Round("6")), 90);
        Frame frame8_Player1 = new Frame(8, jeff, Collections.singletonList(new Round("10")), 120);
        Frame frame9_Player1 = new Frame(9, jeff, Collections.singletonList(new Round("10")), 148);
        Frame frame10_Player1 = new Frame(10, jeff, Arrays.asList(new Round("10"), new Round("8"), new Round("1")), 167);

        Frame frame1_Player2 = new Frame(1, daniel, Arrays.asList(new Round("3"), new Round("7")), 17);
        Frame frame2_Player2 = new Frame(2, daniel, Arrays.asList(new Round("6"), new Round("3")), 30);
        Frame frame3_Player2 = new Frame(3, daniel, Collections.singletonList(new Round("10")), 37);
        Frame frame4_Player2 = new Frame(4, daniel, Arrays.asList(new Round("8"), new Round("1")), 57);
        Frame frame5_Player2 = new Frame(5, daniel, Collections.singletonList(new Round("10")), 77);
        Frame frame6_Player2 = new Frame(6, daniel, Collections.singletonList(new Round("10")), 105);
        Frame frame7_Player2 = new Frame(7, daniel, Arrays.asList(new Round("9"), new Round("0")), 123);
        Frame frame8_Player2 = new Frame(8, daniel, Arrays.asList(new Round("7"), new Round("3")), 131);
        Frame frame9_Player2 = new Frame(9, daniel, Arrays.asList(new Round("4"), new Round("4")), 151);
        Frame frame10_Player2 = new Frame(10, daniel, Arrays.asList(new Round("10"), new Round("9"), new Round("0")), 167);

        bowlingGameService.addFrameToPlayer(frame1_Player1);
        bowlingGameService.addFrameToPlayer(frame1_Player2);
        bowlingGameService.addFrameToPlayer(frame2_Player1);
        bowlingGameService.addFrameToPlayer(frame2_Player2);
        bowlingGameService.addFrameToPlayer(frame3_Player1);
        bowlingGameService.addFrameToPlayer(frame3_Player2);
        bowlingGameService.addFrameToPlayer(frame4_Player1);
        bowlingGameService.addFrameToPlayer(frame4_Player2);
        bowlingGameService.addFrameToPlayer(frame5_Player1);
        bowlingGameService.addFrameToPlayer(frame5_Player2);
        bowlingGameService.addFrameToPlayer(frame6_Player1);
        bowlingGameService.addFrameToPlayer(frame6_Player2);
        bowlingGameService.addFrameToPlayer(frame7_Player1);
        bowlingGameService.addFrameToPlayer(frame7_Player2);
        bowlingGameService.addFrameToPlayer(frame8_Player1);
        bowlingGameService.addFrameToPlayer(frame8_Player2);
        bowlingGameService.addFrameToPlayer(frame9_Player1);
        bowlingGameService.addFrameToPlayer(frame9_Player2);
        bowlingGameService.addFrameToPlayer(frame10_Player1);
        bowlingGameService.addFrameToPlayer(frame10_Player2);


    }

    @Test
    public void calculateScore_fullGame() {
        List<Frame> player1Frame = BowlingGameService.getPlayerFrames(bowlingGame, player1);
        int player1FinalScoreExpected = finalScore(player1Frame);
        cleanScores(player1Frame);

        List<Frame> player2Frame = BowlingGameService.getPlayerFrames(bowlingGame, player2);
        int player2FinalScoreExpected = finalScore(player2Frame);
        cleanScores(player2Frame);

        BowlingGame actualGame = scoreService.calculateGameScore(bowlingGame);

        int player1FinalScoreActual = finalScore(BowlingGameService.getPlayerFrames(bowlingGame, player1));
        int player2FinalScoreActual = finalScore(BowlingGameService.getPlayerFrames(bowlingGame, player1));

        assertEquals("The expect final score for " + player1
                        + " must have been " + player1FinalScoreExpected
                        + " but it was " + player1FinalScoreActual,
                player1FinalScoreExpected, player1FinalScoreActual);

        assertEquals("The expect final score for " + player2
                        + " must have been " + player2FinalScoreExpected
                        + " but it was " + player2FinalScoreActual,
                player2FinalScoreExpected, player2FinalScoreActual);

    }

    @Test
    public void perfectScoreTest() {
        bowlingGame = new BowlingGame();
        this.bowlingGameService = new BowlingGameServiceImpl(bowlingGame);

        Player player3 = new Player("Test User 3");
        Frame frame1_Player1 = new Frame(1, player3, Collections.singletonList(new Round("10")), 30);
        Frame frame2_Player1 = new Frame(2, player3, Collections.singletonList(new Round("10")), 60);
        Frame frame3_Player1 = new Frame(3, player3, Collections.singletonList(new Round("10")), 90);
        Frame frame4_Player1 = new Frame(4, player3, Collections.singletonList(new Round("10")), 120);
        Frame frame5_Player1 = new Frame(5, player3, Collections.singletonList(new Round("10")), 150);
        Frame frame6_Player1 = new Frame(6, player3, Collections.singletonList(new Round("10")), 180);
        Frame frame7_Player1 = new Frame(7, player3, Collections.singletonList(new Round("10")), 210);
        Frame frame8_Player1 = new Frame(8, player3, Collections.singletonList(new Round("10")), 240);
        Frame frame9_Player1 = new Frame(9, player3, Collections.singletonList(new Round("10")), 260);
        Frame frame10_Player1 = new Frame(10, player3, Collections.singletonList(new Round("10")), 270);

        bowlingGameService.addFrameToPlayer(frame1_Player1);
        bowlingGameService.addFrameToPlayer(frame2_Player1);
        bowlingGameService.addFrameToPlayer(frame3_Player1);
        bowlingGameService.addFrameToPlayer(frame4_Player1);
        bowlingGameService.addFrameToPlayer(frame5_Player1);
        bowlingGameService.addFrameToPlayer(frame6_Player1);
        bowlingGameService.addFrameToPlayer(frame7_Player1);
        bowlingGameService.addFrameToPlayer(frame8_Player1);
        bowlingGameService.addFrameToPlayer(frame9_Player1);
        bowlingGameService.addFrameToPlayer(frame10_Player1);

        bowlingGameService.addPlayerToGame(player3);

        List<Frame> player1Frame = BowlingGameService.getPlayerFrames(bowlingGame, player3.getName());
        int player1FinalScoreExpected = finalScore(player1Frame);
        cleanScores(player1Frame);

        BowlingGame actualGame = scoreService.calculateGameScore(bowlingGame);


        int player1FinalScoreActual = finalScore(BowlingGameService.getPlayerFrames(bowlingGame, player3.getName()));

        assertEquals("The expect final score for " + player3.getName()
                        + " must have been " + player1FinalScoreExpected
                        + " but it was " + player1FinalScoreActual,
                player1FinalScoreExpected, player1FinalScoreActual);

    }

    @Test
    public void zeroScoreTest() {
        bowlingGame = new BowlingGame();
        this.bowlingGameService = new BowlingGameServiceImpl(bowlingGame);

        Player player3 = new Player("Test User 3");
        Frame frame1_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame2_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame3_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame4_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame5_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame6_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame7_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame8_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame9_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);
        Frame frame10_Player1 = new Frame(1, player3, Arrays.asList(new Round("0"), new Round("0")), 0);


        bowlingGameService.addFrameToPlayer(frame1_Player1);
        bowlingGameService.addFrameToPlayer(frame2_Player1);
        bowlingGameService.addFrameToPlayer(frame3_Player1);
        bowlingGameService.addFrameToPlayer(frame4_Player1);
        bowlingGameService.addFrameToPlayer(frame5_Player1);
        bowlingGameService.addFrameToPlayer(frame6_Player1);
        bowlingGameService.addFrameToPlayer(frame7_Player1);
        bowlingGameService.addFrameToPlayer(frame8_Player1);
        bowlingGameService.addFrameToPlayer(frame9_Player1);
        bowlingGameService.addFrameToPlayer(frame10_Player1);

        bowlingGameService.addPlayerToGame(player3);

        List<Frame> player1Frame = BowlingGameService.getPlayerFrames(bowlingGame, player3.getName());
        int player1FinalScoreExpected = finalScore(player1Frame);
        cleanScores(player1Frame);

        BowlingGame actualGame = scoreService.calculateGameScore(bowlingGame);


        int player1FinalScoreActual = finalScore(BowlingGameService.getPlayerFrames(bowlingGame, player3.getName()));

        assertEquals("The expect final score for " + player3.getName()
                        + " must have been " + player1FinalScoreExpected
                        + " but it was " + player1FinalScoreActual,
                player1FinalScoreExpected, player1FinalScoreActual);
    }

    @Test
    public void isStrike() {
        List<Round> rounds = new ArrayList<>();
        rounds.add(new Round("10"));

       boolean isStrike = ScoreFunctions.isStrike(rounds);
       assertTrue(isStrike);

    }

    @Test
    public void isSpare() {
        List<Round> rounds = new ArrayList<>();
        rounds.add(new Round("7"));
        rounds.add(new Round("/"));

        boolean isSpare = ScoreFunctions.isSpare(rounds);
        assertTrue(isSpare);

    }

    private int finalScore(List<Frame> frames) {
        return frames.get(frames.size() - 1).getTotalScore();
    }

    private void cleanScores(List<Frame> frames) {
        //Clean scores
        frames.forEach(set -> set.setTotalScore(0));
    }
}
