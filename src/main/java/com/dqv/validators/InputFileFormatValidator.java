package com.dqv.validators;

import com.dqv.exceptions.InputFileInvalidFormat;
import lombok.val;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 14:34
 */

public class InputFileFormatValidator implements Validator {

    @Override
    public boolean isValid(String content) throws InputFileInvalidFormat {
        if (content == null || content.isEmpty()) {
            return false;
        }

        val matchPattern = Pattern.compile("^(?<Player>\\w+)(?<Tab>\t)(?<Points>\\d{1,2}|[FX/])$");
        Matcher matcher = null;
        for (String line : content.split("\n")) {
            matcher = matchPattern.matcher(line);
            if (!matcher.matches()) {
                throw new InputFileInvalidFormat();
            }
            String points = matcher.group("Points");
            if (!"FX/".contains(points) && (Integer.parseInt(points) < 0 || Integer.parseInt(points) > 10)) {
                return false;
            }
        }
        return true;
    }
}
