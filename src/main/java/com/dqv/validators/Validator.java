package com.dqv.validators;

import com.dqv.exceptions.InputFileInvalidFormat;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 14:35
 */
public interface Validator {

    boolean isValid(String content) throws InputFileInvalidFormat;

}
