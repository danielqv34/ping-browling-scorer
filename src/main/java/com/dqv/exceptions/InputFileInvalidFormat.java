package com.dqv.exceptions;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 14:31
 */
/**
 * Class to capture a invalid format file
 */
public class InputFileInvalidFormat extends RuntimeException {
    private final String MESSAGE_ERR = "The information in the file have a invalid format";


    public InputFileInvalidFormat() {
    }

    @Override
    public String getMessage() {
        return MESSAGE_ERR;
    }

}
