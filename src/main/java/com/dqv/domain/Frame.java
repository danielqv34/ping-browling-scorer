package com.dqv.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 17:18
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"frameId", "player"})
public class Frame {

    private int frameId;

    private Player player;

    private List<Round> rounds = new ArrayList<>();

    private int totalScore;

    public Frame(int frameId, Player player) {
        this.frameId = frameId;
        this.player = player;
    }

    public Frame(Player player, List<Round> rounds) {
        this.player = player;
        this.rounds = rounds;
    }

    public Frame(int frameId, Player player, List<Round> rounds) {
        this.frameId = frameId;
        this.player = player;
        this.rounds = rounds;
    }


}
