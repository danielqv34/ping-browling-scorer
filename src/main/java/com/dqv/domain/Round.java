package com.dqv.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 17:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Round {

    private String score;

}
