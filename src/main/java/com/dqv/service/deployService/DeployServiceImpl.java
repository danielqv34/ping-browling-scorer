package com.dqv.service.deployService;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.service.bowlingGameService.BowlingGameService;
import com.dqv.service.scoreService.ScoreFunctions;
import lombok.val;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 17:59
 * <p>
 * Class to print out on console the game results
 */
public class DeployServiceImpl implements DeployService {


    @Override
    public void deploy(BowlingGame bowlingGame) {
        if (bowlingGame != null) {
            val framesInitCount = bowlingGame.getFrames().size() / bowlingGame.getPlayers().size();
            System.out.println(("Frame\t\t" +
                    IntStream.rangeClosed(1, framesInitCount)
                            .boxed()
                            .map(i -> i + "\t\t")
                            .collect(Collectors.joining())).trim()
            );

            bowlingGame
                    .getPlayers()
                    .forEach(player -> {
                        val frames = BowlingGameService.getPlayerFrames(bowlingGame, player.getName());
                        System.out.println(player.getName());
                        System.out.println(
                                ("Pinfalls\t" +
                                        (frames
                                                .stream()
                                                .map(this::formatPrinterRounds)
                                                .collect(Collectors.joining())))
                                        .trim()
                        );

                        System.out.println((
                                        "Score\t\t" + (
                                                frames
                                                        .stream()
                                                        .map(this::scoreFormatter)
                                                        .collect(Collectors.joining()))
                                                .trim()
                                )
                        );
                    });
        } else {
            System.out.println("Not valid game to initialized");
        }
    }

    private String formatPrinterRounds(Frame frame) {
        val result = new StringBuilder();
        if (ScoreFunctions.isStrike(frame.getRounds())) {
            result.append("\tX\t");
        } else if (ScoreFunctions.isSpare(frame.getRounds())) {
            result
                    .append(frame.getRounds().get(0).getScore())
                    .append("\t/\t");
        } else {
            frame.getRounds()
                    .stream()
                    .filter(Objects::nonNull)
                    .forEach(round -> result.append(round.getScore()).append("\t"));
        }
        return result.toString();
    }

    private String scoreFormatter(Frame frame) {
        return frame.getTotalScore() + "\t\t";
    }
}
