package com.dqv.service.deployService;

import com.dqv.domain.BowlingGame;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 17:58
 */
public interface DeployService {

    void deploy(BowlingGame bowlingGame);
}
