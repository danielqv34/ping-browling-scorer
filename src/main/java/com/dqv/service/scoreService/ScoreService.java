package com.dqv.service.scoreService;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;

import java.util.List;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 11:19
 */
public interface ScoreService {

    BowlingGame calculateGameScore(BowlingGame bowlingGame);

    int calculatePlayerScoreByFrame(List<Frame> frames);



}
