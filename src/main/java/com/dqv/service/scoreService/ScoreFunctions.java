package com.dqv.service.scoreService;

import com.dqv.domain.Round;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 17:31
 */
public interface ScoreFunctions {

    static boolean isStrike(List<Round> rounds) {
        return rounds != null &&
                cleanInvalidRounds(rounds).size() == 1 &&
                (rounds.get(0).getScore().equals("X") || rounds.get(0).getScore().equals("10"));
    }

    static boolean isSpare(List<Round> rounds) {
        return rounds != null &&
                cleanInvalidRounds(rounds).size() == 2 &&
                (rounds.get(1).getScore().equals("/") ||
                        roundClearValue(2, rounds) == 10);
    }

    static List<Round> cleanInvalidRounds(List<Round> rounds) {
        return rounds
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


    static int roundClearValue(Round round) {
        return round.getScore() == null ? 0 :
                round.getScore().equals("X") ? 10 :
                        round.getScore().equals("F") ? 0 :
                                round.getScore().equals("/") ? 0 :
                                        Integer.parseInt(round.getScore());
    }

    static int roundClearValue(int countToSum, List<Round> rounds) {
        return rounds == null ? 0 : rounds
                .stream()
                .limit(countToSum)
                .map(ScoreFunctions::roundClearValue)
                .reduce(0, Integer::sum);
    }

    static boolean isFrameEnded(int currentFrame, List<Round> rounds) {
        if(currentFrame == 10){
            return false;
        }
        List<Round> cleanedRounds = cleanInvalidRounds(rounds);
        if(cleanedRounds.size() == 3){
            return true;
        }else if(cleanedRounds.size() == 2 && roundClearValue(rounds.get(0)) != 10){
            return true;
        }else if(roundClearValue(rounds.get(0)) == 10){
            return true;
        }else{
            return false;
        }

    }
}
