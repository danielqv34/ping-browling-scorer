package com.dqv.service.scoreService;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import lombok.val;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 16:42
 */

public class ScoreServiceImpl implements ScoreService {

    @Override
    public BowlingGame calculateGameScore(BowlingGame bowlingGame) {
        if (bowlingGame != null) {
            if (bowlingGame.getFrames() != null && bowlingGame.getPlayers() != null) {
                bowlingGame.getPlayers()
                        .forEach(player -> {
                            val frames = bowlingGame.getFrames()
                                    .stream()
                                    .filter(frame -> frame.getPlayer() != null && frame.getPlayer().equals(player))
                                    .collect(Collectors.toList());
                            calculatePlayerScoreByFrame(frames);
                        });
            }
        }
        return bowlingGame;
    }

    @Override
    public int calculatePlayerScoreByFrame(List<Frame> frames) {
        if (frames != null && !frames.isEmpty()) {
            int previousFrameScore = 0;
            for (int i = 0; i < frames.size(); i++) {
                Frame currentFrame = frames.get(i);
                int currentFrameScore = previousFrameScore;
                if (ScoreFunctions.isStrike(currentFrame.getRounds())) {
                    currentFrameScore += 10;
                    if ((i + 1) < frames.size()) {
                        Frame nextFrame = frames.get(i + 1);
                        if (ScoreFunctions.isStrike(nextFrame.getRounds())) {
                            currentFrameScore += 10;
                            if ((i + 2) < frames.size()) {
                                Frame next2Frames = frames.get(i + 2);
                                currentFrameScore += ScoreFunctions.roundClearValue(next2Frames.getRounds().get(0));
                            }
                        } else if (ScoreFunctions.isSpare(nextFrame.getRounds())) {
                            currentFrameScore += 10;
                        } else {
                            currentFrameScore += ScoreFunctions.roundClearValue(2, nextFrame.getRounds());
                        }
                    }
                } else if (ScoreFunctions.isSpare(currentFrame.getRounds())) {
                    currentFrameScore += 10;
                    if ((i + 1) < frames.size()) {
                        Frame nextFrame = frames.get(i + 1);
                        currentFrameScore += ScoreFunctions.roundClearValue(nextFrame.getRounds().get(0));
                    }
                } else {
                    currentFrameScore += ScoreFunctions.roundClearValue(currentFrame.getRounds().size(),
                            currentFrame.getRounds());
                }

                currentFrame.setTotalScore(currentFrameScore);

                previousFrameScore = currentFrameScore;
            }
            return frames.get(frames.size() - 1).getTotalScore();
        } else {
            return -1;
        }
    }
}
