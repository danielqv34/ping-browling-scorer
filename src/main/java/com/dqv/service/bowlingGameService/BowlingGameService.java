package com.dqv.service.bowlingGameService;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.domain.Player;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 19:24
 */
public interface BowlingGameService {

    Set<Player> getAllPlayers();

    Set<Frame> getAllFrames();

    void addPlayerToGame(Player player);

    void addFrameToPlayer(Frame frame);

    /**
     * Method to get all frames by player on the game
     */
    static List<Frame> getPlayerFrames(BowlingGame bowlingGame, String playerName) {
        return bowlingGame.getFrames()
                .stream()
                .filter(frame -> frame.getPlayer().getName().equals(playerName))
                .collect(Collectors.toList());
    }

}
