package com.dqv.service.bowlingGameService;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.domain.Player;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 19:24
 */
public class BowlingGameServiceImpl implements BowlingGameService {


    private final BowlingGame bowlingGame;

    public BowlingGameServiceImpl(BowlingGame bowlingGame) {
        this.bowlingGame = bowlingGame;
    }

    @Override
    public Set<Player> getAllPlayers() {
        return this.bowlingGame.getPlayers();
    }

    @Override
    public Set<Frame> getAllFrames() {
        return this.bowlingGame.getFrames();
    }

    @Override
    public void addPlayerToGame(Player player) {
        if (player != null) {
            this.bowlingGame.getPlayers().add(player);
        } else {
            this.bowlingGame.setPlayers(Collections.emptySet());
        }
    }

    @Override
    public void addFrameToPlayer(Frame frame) {
        if (frame != null) {
            this.bowlingGame.getFrames().add(frame);
        } else {
            this.bowlingGame.setFrames(new LinkedHashSet<>());
        }
    }
}
