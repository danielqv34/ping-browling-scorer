package com.dqv.service.bowlingGameService;

import com.dqv.domain.BowlingGame;
import com.dqv.service.deployService.DeployService;
import com.dqv.service.fileReaderService.CustomFileReaderService;
import com.dqv.service.parser.BowlingGameParser;
import com.dqv.service.scoreService.ScoreService;
import com.dqv.validators.Validator;
import lombok.val;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 17:45
 */
public class LoadBowlingGameService implements InitGameService {


    private CustomFileReaderService fileReaderService;
    private Validator validator;
    private BowlingGameParser bowlingGameParser;
    private ScoreService scoreService;
    private DeployService deployService;

    public LoadBowlingGameService(CustomFileReaderService fileReaderService, Validator validator,
                                  BowlingGameParser bowlingGameParser, ScoreService scoreService,
                                  DeployService deployService) {
        this.fileReaderService = fileReaderService;
        this.validator = validator;
        this.bowlingGameParser = bowlingGameParser;
        this.scoreService = scoreService;
        this.deployService = deployService;
    }

    @Override
    public Optional<BowlingGame> initGame(String filePath) {
        try {
            val fileContent = fileReaderService.readFile(fileReaderService.getFile(filePath));
            if (validator.isValid(fileContent)) {
                val gameInitValue = bowlingGameParser.bowlingGameParser(Arrays.asList(fileContent.split("\n")));
                if (!gameInitValue.isPresent()) {
                    System.out.println("Cannot be parse the file");
                }
                return gameInitValue;
            }

        } catch (IOException ex) {
            System.out.println("Error reading the file: {}" + ex.getMessage());
        }
        return Optional.of(new BowlingGame());
    }

    @Override
    public BowlingGame getGameScores(BowlingGame bowlingGame) {
        return scoreService.calculateGameScore(bowlingGame);
    }

    @Override
    public void deployGameResults(BowlingGame bowlingGame) {
        deployService.deploy(bowlingGame);
    }
}
