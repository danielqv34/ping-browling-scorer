package com.dqv.service.bowlingGameService;

import com.dqv.domain.BowlingGame;

import java.util.Optional;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 17:46
 */
public interface InitGameService {

    Optional<BowlingGame> initGame(String filePath);
    BowlingGame getGameScores(BowlingGame bowlingGame);
    void deployGameResults(BowlingGame bowlingGame);
}
