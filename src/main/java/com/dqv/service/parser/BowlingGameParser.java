package com.dqv.service.parser;

import com.dqv.domain.BowlingGame;

import java.util.List;
import java.util.Optional;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 00:12
 */
public interface BowlingGameParser {
    Optional<BowlingGame> bowlingGameParser(List<String> fileLines);
}
