package com.dqv.service.parser;

import com.dqv.domain.BowlingGame;
import com.dqv.domain.Frame;
import com.dqv.domain.Player;
import com.dqv.domain.Round;
import com.dqv.service.scoreService.ScoreFunctions;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-25
 * Time: 00:13
 */

public class BowlingGameParserImpl implements BowlingGameParser {

    @Override
    public Optional<BowlingGame> bowlingGameParser(List<String> fileLines) {

        if (fileLines == null || fileLines.isEmpty()) {
            return Optional.empty();
        }
        Set<Frame> frames = new LinkedHashSet<>();

        String firstPlayer = fileLines.get(0).split("\\t")[0];
        int currentFrame = 1;
        boolean isMultiPlayer = false;
        Frame currentPlayerFrame = null;

        for (String line : fileLines) {
            val values = line.split("\t");
            if (!values[0].equals(firstPlayer)) {
                isMultiPlayer = true;
            }

            if (currentPlayerFrame != null
                    && (isMultiPlayer && !values[0].equals(currentPlayerFrame.getPlayer().getName()) && values[0].equals(firstPlayer)
                    || (!isMultiPlayer && ScoreFunctions.isFrameEnded(currentFrame, currentPlayerFrame.getRounds())))) {
                currentFrame++;
            }

            Frame tempFrame = new Frame(currentFrame, new Player(values[0]));
            if (frames.contains(tempFrame)) {
                currentPlayerFrame = frames.stream()
                        .filter(set -> set.equals(tempFrame))
                        .findFirst()
                        .orElse(tempFrame);
            } else {
                currentPlayerFrame = tempFrame;
            }
            currentPlayerFrame.getRounds().add(new Round(values[1]));
            frames.add(currentPlayerFrame);

        }
        Set<Player> players = frames.stream()
                .map(Frame::getPlayer)
                .collect(Collectors.toSet());

        return Optional.of(new BowlingGame(players, frames));
    }
}
