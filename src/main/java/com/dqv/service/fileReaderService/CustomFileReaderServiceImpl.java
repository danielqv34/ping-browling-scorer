package com.dqv.service.fileReaderService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 20:00
 */
public class CustomFileReaderServiceImpl implements CustomFileReaderService {


    @Override
    public File getFile(String uri) {
        return new File(uri);
    }

    @Override
    public String readFile(File file) throws IOException {
        return bufferedReader(file).lines().collect(Collectors.joining("\n"));
    }

    @Override
    public List<String> readFilePerLine(File file) throws IOException {
        return bufferedReader(file).lines().collect(Collectors.toList());
    }


    private BufferedReader bufferedReader(File file) throws FileNotFoundException {
        return new BufferedReader(new FileReader(file));
    }

}
