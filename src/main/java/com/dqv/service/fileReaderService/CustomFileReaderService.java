package com.dqv.service.fileReaderService;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 20:00
 */
public interface CustomFileReaderService {

    File getFile(String uri);
    String readFile(File file) throws IOException;
    List<String> readFilePerLine(File file)  throws IOException;
}
