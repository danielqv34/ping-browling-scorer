package com.dqv;

import com.dqv.service.bowlingGameService.LoadBowlingGameService;
import com.dqv.service.deployService.DeployServiceImpl;
import com.dqv.service.fileReaderService.CustomFileReaderServiceImpl;
import com.dqv.service.parser.BowlingGameParserImpl;
import com.dqv.service.scoreService.ScoreServiceImpl;
import com.dqv.validators.InputFileFormatValidator;
import lombok.val;

/**
 * Created in IntelliJ IDEA.
 *
 * @author: Daniel QuirozV
 * Date: 2019-08-24
 * Time: 17:10
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Initialized Top-Ten Bowling Scoreboard");
        LoadBowlingGameService loadBowlingGameService = new LoadBowlingGameService(
                new CustomFileReaderServiceImpl(), new InputFileFormatValidator(), new BowlingGameParserImpl(),
                new ScoreServiceImpl(), new DeployServiceImpl()
        );
        if (args.length != 0) {
            val bowlingGameInit = loadBowlingGameService.initGame(args[0]);
            bowlingGameInit.ifPresent(bowlingGame -> {
                loadBowlingGameService.getGameScores(bowlingGame);
                loadBowlingGameService.deployGameResults(bowlingGame);
            });
        } else {
            System.out.println("A input file .txt is required to init the game");
        }

    }
}
